module PolinomisC1

using Polynomial

export arrels
export divisors
export ruffini
export grau2
#arrels = p -> filter(isreal, roots(p)) #On posa arrels abans posava roots.

function divisors (p)#retorna els divisors del termin independent i també els mateixos pero amb signe negatiu per fer ruffini

        ultim = length (p)
        independent = p[ultim]
        n = 1
        div = [1, -1]

        while n <= independent
                if independent % n == 0
                        if n != 1
                             push! (div, n)
                             push! (div, -n)
                        end
                end
                n = n+1
        end

        return div

end

function ruffini (p)
        divneg = divisors (p)
        push!
        return div
end

function grau2 (p)
        a = p[1]
        b = p[2]
        c = p[3]

        x1 = (-b+sqrt(b^2-4*a*c))/(2*a)
        x2 = (-b-sqrt(b^2-4*a*c))/(2*a)

        resultat = [x1, x2]

end


function arrels (p)
        resultat = Int64[]

    if length(p)==2 #Polinomi grau 1
            a = p[1]
            b = p[2]

            x = -b/a
            resultat = [x]
    end

    if length(p)==3 #Polinomi grau 2

            a = p[1]
            b = p[2]
            c = p[3]


            if (b^2-4*a*c) >= 0

            x1 = (-b+sqrt(b^2-4*a*c))/(2*a)
            x2 = (-b-sqrt(b^2-4*a*c))/(2*a)

            resultat = [x1, x2]

           end

           if (b^2-4*a*c) < 0

           return resultat

          end

    end

    if length(p)==4 #Polinomi grau 3
            a = (p[2]/p[1])
            b = (p[3]/p[1])
            c = (p[4]/p[1])

            Q=(a*a-3*b)/9
            R=(2*a^3-9*a*b+27*c)/54


                    angle = acos(R/sqrt(Q^3))
                    x1=-2*sqrt(Q)*cos(angle/3)-a/3
                    x2=-2*sqrt(Q)*cos((angle+2*pi)/3)-a/3
                    x3=-2*sqrt(Q)*cos((angle-2*pi)/3)-a/3



            resultat = [x1, x2, x3]

    end

return resultat

end

end #module
